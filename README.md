# The Kaplan Drupal test #


There are many ways to complete this test, feel free to use the methods that you think are best.


### Instructions ###

To begin the test you must first fork this repository. Later please submit your work as a pull request to the branch "test-complete".

Try and deliver your code in the most deployable fashion. Also provide a DOCUMENTATION.txt file with instructions on how to get your changes up and running from a clean installation.


### Requirements ###

1. Create a content type called "kaplan". This should have three fields: Title, Body and Message (text).
2. Create a node of type "kaplan".
3. On a page located at '/kaplan-test' display the title of one "kaplan" node and it's body.
4. Integrate the [qTip2](http://qtip2.com/) javascript library with the site.
5. Have the contents of the "Message" field appear in a tooltip when you hover over the displayed body.

The image below is a rough mock-up of what the result should be.

![kaplan_test_compact.png](https://bitbucket.org/repo/6doAzo/images/3002059570-kaplan_test_compact.png)


### Notes ###

* Custom modules welcome, we like code.
* Feel free to use contrib modules where necessary, eg. for bundling and integration. However please refrain from using modules for displaying the content such as Views, Display Suite and Panels.
* Don't worry about theming, using the standard Drupal theme is fine.
* qTip2 requires jQuery 1.6